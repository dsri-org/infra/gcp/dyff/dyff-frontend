# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

output "basic_auth_credentials" {
  sensitive = true
  value     = local.basic_auth_credentials
}

output "basic_auth_htpasswd_file" {
  sensitive = true
  value     = local.basic_auth_htpasswd_file
}
