# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  cluster_name = "${var.environment}-dyff-cloud"
  deployment   = "dyff-frontend"
  name         = "${var.environment}-${local.deployment}"
  region       = "us-central1"

  default_tags = {
    deployment  = local.deployment
    environment = var.environment
  }

  basic_auth_users = [
    "dyff",
  ]

  basic_auth_credentials = [
    for user in local.basic_auth_users : {
      username = user
      password = random_pet.basic_auth[user].id
    }
  ]

  # https://httpd.apache.org/docs/2.4/misc/password_encryptions.html
  # https://developer.hashicorp.com/terraform/language/functions/bcrypt
  basic_auth_htpasswd_file = <<-EOF
%{for user in local.basic_auth_users~}
${user}:${bcrypt(random_pet.basic_auth[user].id)}
%{endfor~}
EOF
}
