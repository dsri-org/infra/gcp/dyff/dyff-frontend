# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

variable "environment" {
  type = string
}

variable "google_cloud_service_account_file" {
  type = string
}

variable "project" {
  type = string
}

variable "hostname" {
  type = string
}

variable "api_hostname" {
  type = string
}
