# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "kubernetes_namespace" "dyff_frontend" {
  metadata {
    name = "dyff-frontend"
    labels = {
      # https://kubernetes.io/docs/concepts/security/pod-security-standards/
      "pod-security.kubernetes.io/enforce" = "restricted"
    }
  }
}

resource "random_pet" "basic_auth" {
  for_each = toset(local.basic_auth_users)

  length    = 4
  separator = " "
}

resource "kubernetes_secret" "basic_auth" {
  metadata {
    name      = "basic-auth"
    namespace = kubernetes_namespace.dyff_frontend.metadata[0].name
  }

  data = {
    auth = local.basic_auth_htpasswd_file
  }
}

resource "helm_release" "dyff_frontend" {
  name       = "dyff-frontend"
  namespace  = kubernetes_namespace.dyff_frontend.metadata[0].name
  repository = "oci://registry.gitlab.com/dyff/charts"
  version    = "0.13.7"
  chart      = "dyff-frontend"

  wait          = false
  wait_for_jobs = false

  values = [yamlencode({
    # no-op autoscaling until resources key is populated
    autoscaling = {
      enabled     = true
      maxReplicas = 3
      minReplicas = 3
    }

    extraEnvVarsConfigMap = {
      DYFF_API_FULL_URL  = "https://${var.api_hostname}/v0"
      DYFF_API_AUTH_URL  = "https://${var.api_hostname}/auth"
      DYFF_API_VIEWS_URL = "https://${var.api_hostname}/views"
    }

    ingress = {
      enabled = false
    }

    topologySpreadConstraints = [{
      labelSelector = {
        matchLabels = {
          "app.kubernetes.io/instance" = "dyff-frontend"
          "app.kubernetes.io/name"     = "dyff-frontend"
        }
      }
      maxSkew           = 1
      topologyKey       = "topology.kubernetes.io/zone"
      whenUnsatisfiable = "DoNotSchedule"
    }]
  })]
}


locals {
  ingress_path = {
    pathType = "ImplementationSpecific"
    backend = {
      service = {
        name = helm_release.dyff_frontend.name
        port = {
          number = 3000
        }
      }
    }
  }

  ingress_tls = [{
    hosts      = [var.hostname]
    secretName = "dyff-frontend-tls" # pragma: allowlist secret
  }]

  ingress_dashboard_paths = [
    "/_next.*",
    "/api.*",
    "/auth.*",
    "/dashboard.*",
    "/user.*",
    "/views.*",
    "/static.*",
    "/$",
  ]

  ingress_public_paths = [
    "/home.*",
    "/reports.*",
    "/systems.*",
    "/tests.*",
    "/usecases.*",
  ]
}

resource "kubernetes_manifest" "dyff_frontend_dashboard" {
  manifest = {
    apiVersion = "networking.k8s.io/v1"
    kind       = "Ingress"
    metadata = {
      name      = "dyff-frontend-dashboard"
      namespace = kubernetes_namespace.dyff_frontend.metadata.0.name
      annotations = {
        "cert-manager.io/cluster-issuer"        = "letsencrypt-production"
        "nginx.ingress.kubernetes.io/use-regex" = "true"
      }
    }
    spec = {
      ingressClassName = "nginx"
      rules = [{
        host = var.hostname
        http = {
          paths = [
            for path in local.ingress_dashboard_paths : merge(local.ingress_path, {
              path = path
            })
          ]
        }
      }]
      tls = local.ingress_tls
    }
  }
}

resource "kubernetes_manifest" "dyff_frontend_public" {
  manifest = {
    apiVersion = "networking.k8s.io/v1"
    kind       = "Ingress"
    metadata = {
      name      = "dyff-frontend-public"
      namespace = kubernetes_namespace.dyff_frontend.metadata.0.name
      annotations = {
        "cert-manager.io/cluster-issuer"        = "letsencrypt-production"
        "nginx.ingress.kubernetes.io/use-regex" = "true"

        # uncomment to enable basic auth
        "nginx.ingress.kubernetes.io/auth-realm"  = "Authentication required to access Dyff"
        "nginx.ingress.kubernetes.io/auth-secret" = kubernetes_secret.basic_auth.metadata.0.name
        "nginx.ingress.kubernetes.io/auth-type"   = "basic"
      }
    }
    spec = {
      ingressClassName = "nginx"
      rules = [{
        host = var.hostname
        http = {
          paths = [
            for path in local.ingress_public_paths : merge(local.ingress_path, {
              path = path
            })
          ]
        }
      }]
      tls = local.ingress_tls
    }
  }
}
