# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/hashicorp/google" {
  version     = "5.18.0"
  constraints = "~> 5.18.0"
  hashes = [
    "h1:rwT/Nbyt86WhxJMS24UhECIfe2oYAMEO+MaXk0Uf9ak=",
    "zh:02e755030e5ea87eefda0f8bb3e6b53df2d2578144c92b4daa21d42336386b83",
    "zh:25c3eb7ec7654f4955abcc700d91e9bc7f0ef196508702194351a97dabc087b9",
    "zh:6dc9014d91bcbb21546e561fe465aed9c8a69b4d465414f8723026b4b47abaea",
    "zh:876fcb3c32a72b21e61fcfac34cc6749edb7247628165eda2e2a3f0282f8e6a0",
    "zh:ac6d119350be3a4463cff3f7b6c7d86abeaee279d01c9c344571eff7d945180a",
    "zh:c28036b74c81007770fe97aae594e2d797eeff51a4807bb37be3891df3109194",
    "zh:d06bac750620247344ea2d57196bdd8e1daed68fe0005a028d059e89116dfe31",
    "zh:d4dbeddfe7bb80e0df8d26ff706d8c897e3e82698f3402b2659050704c0789e0",
    "zh:d604b24b5001da7743f4ce5036e4ef17f7cc54478f7cb327144cef3760e05bf3",
    "zh:e1b8a575928c5bbaa7afa7267474a1bc52da4e9ac526343dfbcc3ef95584fe61",
  ]
}

provider "registry.opentofu.org/hashicorp/helm" {
  version     = "2.11.0"
  constraints = "~> 2.11.0"
  hashes = [
    "h1:ohKpsfAdRWWQZpqbK99sol2Pbp7betwUKUSZksVjI3s=",
    "zh:2b505116e91325a26a0749ad9820ec4f2a9f47e195a84d6fd1f41d86b92bdefd",
    "zh:2f44100e7e6c1063a3c872066a04378b7336d5b4fbc1f6581d3e69f85dbbba3f",
    "zh:71fec2cda63d13250cf460e48e8a1e8f3200f93f15e5ed02df871fe86d6c559c",
    "zh:7a2b5c182ba8bb980a44642f475334eca8e5e689083003471d0f8272482bb209",
    "zh:9cddaf003b66530a02b2efb574ef9fecaad5a8f00ea744e435de5c6672a14a22",
    "zh:ac6cb892a2eeddae58548c9c252e266a07df7f8b894670b68729c7eae562ac92",
    "zh:ad88f8d7390ce32204b01cd74133c7ed85fbc5dab2add6c6784ce52be7a99cae",
    "zh:cc7e007ea23739ebe4efb75bf2fae2c95aa18c63ea109357d7f8f79940182f61",
    "zh:da97c83e849c74da08bb1ed70edd062db1b2469fe48fb7d44399baecba8c1009",
    "zh:f94ce933127a56d3f6019e483a1f6f5edb2255e5edfed99ecfac26e444995152",
  ]
}

provider "registry.opentofu.org/hashicorp/kubernetes" {
  version     = "2.27.0"
  constraints = "~> 2.27.0"
  hashes = [
    "h1:Jtbdvbq8kIXUENtH3tVwgcjHqbuYp1pGfg4gFocY+e4=",
    "zh:1146f53fb39fd4bcea5574303c4871001a97d7891f65a60a4ecbc64da2a90d75",
    "zh:1f7e3dc0dbb854f56a0f5ba3c50588272984ae9775da027c3c7f32cb6d8245b0",
    "zh:2166f7fdade75266658603280bc822edab848e52a674340485847dde1c5d9324",
    "zh:21a97530857330d2013aa66fb7afebb44fe4a5543418d0a3ca93750acd11fea5",
    "zh:2d4b9fea7e99750647e1cd8df9a67cba45905825867dd19ab01411dad6b8c6fd",
    "zh:de30e92e638b95e56dbb2232cb9a6f6a69346ecb3644965e9be715eaf29f22ff",
    "zh:f4ae951c9add4349a498f44c3f5768cbaf7a966392a0e7632de288889e7cd5d9",
    "zh:f54ecb1917dfa198933d72632ea6f0aa4da3ead070d6b9765ec1d3b7da60e827",
    "zh:fba8a2f192eb5fe248708b9037db046e0d9176e7c54c6edc6f6aa55d50474082",
    "zh:fe525956f3e54f0bbd2891a6abad1f807b4763b8dc734d810e223876741fefa3",
  ]
}

provider "registry.opentofu.org/hashicorp/random" {
  version     = "3.6.2"
  constraints = "~> 3.6.2"
  hashes = [
    "h1:PXvoOj9gj+Or+9k0tQWCQJKxnsVO0GqnQwVahgwRrsU=",
    "zh:1f27612f7099441526d8af59f5b4bdcc35f46915df5d243043d7337ea5a3e38a",
    "zh:2a58e66502825db8b4b96116c04bd0323bca1cf1f5752bdd8f9c26feb84d3b1e",
    "zh:4f0a4fa479e29de0c3c90146fd58799c097f7a55401cb00560dd4e9b1e6fad9d",
    "zh:9c93c0fe6ef685513734527e0c8078636b2cc07591427502a7260f4744b1af1d",
    "zh:a466ff5219beb77fb3b18a3d7e7fe30e7edd4d95c8e5c87f4f4e3fe3eeb8c2d7",
    "zh:ab33e6176d0c757ddb31e40e01a941e6918ad10f7a786c8e8e4f35e5cff81c96",
    "zh:b6eabf377a1c12cb3f9ddd97aacdd5b49c1646dc959074124f81d40fcd216d7e",
    "zh:ccec5d03d0d1c0f354be299cdd6a417b2700f1a6781df36bcce77246b2f57e50",
    "zh:d2a7945eeb691fdd2b1474da76ddc2d1655e2aedbb14b57f06d4f5123d47adf9",
    "zh:ed62351f4ad9d1469c6798b77dee5f63b18b29c473620a0046ba3d4f111b621d",
  ]
}
