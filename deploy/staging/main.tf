# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

module "root" {
  source = "../.."

  google_cloud_service_account_file = var.google_cloud_service_account_file
  environment                       = "staging"
  project                           = "staging-dyff-659606"
  api_hostname                      = "staging-api.dyff.io"
  hostname                          = "staging-app.dyff.io"
}
